# gwd-expandible-multiple

Template elaborado con Google Web Designer

##Instalar Demo

Copiar el siguiente código en la consola de windows y abrir el archivo index.html de la carpeta export:

`$ git clone https://Mlagos@bitbucket.org/desarrolladorescrp/gwd-expandible-multiple.git`

##Proceso de creación en Google Web Designer

- Crear el proyecto como se indica en la siguiente imagen:

![](https://bitbucket.org/desarrolladorescrp/gwd-expandible-multiple/raw/12dc299e097740a62aa3b49d01d28da5b6efbc8b/nuevo.PNG)

- Menu Archivo/importar recursos, para poder incluir las imágenes del proyecto. Estas imágenes se ubicaran en el panel "Biblioteca".
- Mantener un orden de capas en la linea de tiempo. Se recomienda siempre colocar las capas de componentes en la parte superior de cualquier capa. Por ejemplo, la capa de componente "área de pulsación", si esta se encontrara debajo de una capa imagen, no hará click.
- Crear dos componentes de "área de pulsación" uno para la página banner y la otra para la página expandida. Además, a cada uno poner un ID como por ejemplo "click-tag-1" y "click-tag-2", esto servirá para añadirle eventos más adelante.
- Situado en la página banner, en el panel propiedades activar el check expandido.
- Al crear el proyecto automáticamente se genera dos componentes de área de pulsación, uno ubicado en la página de banner, que se utiliza para expandir el banner y el otro componente se ubica en la página expandido que se utiliza para replgar el banner. Ambos botones ya cuenta con eventos insertados. 

###Evento para que inicie expandido

- Click en el botón "+" situado en el panel Eventos. 
- Objetivo : document.body
- Evento: Anuncio de google / anuncio inicializado
- Acción: Conjunto de páginas / ir a la página
- Receptor: pagedeck
- Configuración: Identificador de página / Página expandida.
- Aceptar y generar un vista previa.

###Evento para click tag

- Selecionar el componente generado para el clicktag.
- Click en el botón "+" situado en el panel Eventos.
- Evento: Ratón / click
- Acción: Anuncio de google / Salir del anuncio
- Receptor: gwd-ad
- Configuración: ID métricas / colocar un nombre que referente
- Configuración: URL / colocar una URL con https:// de referencia.

##Publicar anuncio
Quitar el check a archivos locales insertados(.js y .css)

![](https://bitbucket.org/desarrolladorescrp/gwd-expandible-multiple/raw/12dc299e097740a62aa3b49d01d28da5b6efbc8b/publicar.PNG)

##Personalizar el banner expandible
- Cortar el código css que se encuentra en el archivo index.html y pegarlo en un nuevo archivo css como por ejemplo multimedia_expandible.css
- En el archivo multimedia_expandible.css añadir el siguiente código debajo de todo.

```html
<style type="text/css">
    #pagedeck {
        width: 300px;
        height: 250px;
    }
    #expanded-page{
        right: 0!important;
        left: initial!important;
        z-index: 200;
    }
</style>
```
- Llamar al multimedia_expandible.css en el header del archivo index.html

Quedaría como este ejemplo:
```html
<link rel="stylesheet" href="multimedia_expandible.css">
```
- Abrir el archivo gwddoubleclick_min.js y realizar los siguientes cambios

```javascript
document.body.style.opacity="0";
/simplemente borrar el 0
document.body.style.opacity="";
```

- Subir las imágenes, los css y js en el S3 y cambiar las rutas relativas a absolutas del archivo index.html
- Cambiar la url click tag demo que se creó en google web designer por el código de trackeo de DFP

```html
<gwd-exit metric="clicktag1" url="%%CLICK_URL_UNESC%%%%DEST_URL%%"></gwd-exit>
<gwd-exit metric="clicktag2" url="%%CLICK_URL_UNESC%%%%DEST_URL%%"></gwd-exit>
<!-- cambiar por-->
<gwd-exit metric="clicktag1" url="%%CLICK_URL_UNESC%%%%DEST_URL%%"></gwd-exit>
<gwd-exit metric="clicktag2" url="%%CLICK_URL_UNESC%%%%DEST_URL%%"></gwd-exit>
```
- Se tiene que reemplazar en todas las url que se creó en el demo. Recomiendo selecionar todo el texto de url y presionar control+D para hacer selección multiple y cambiar el código, eso es para sublime txt.

- En el archivo index.html buscar los siguientes eventos gwd.actions.events.addHandler y gwd.actions.events.removeHandler y cambiar el evento "click" por "action". Esto ayudará a que el evento trackeo funcione en versiones mobile.

```javascript
gwd.actions.events.addHandler("click-tag-1","click",gwd.auto_Click_tag_1Click,false)
gwd.actions.events.addHandler("click-tag-2","click",gwd.auto_Click_tag_2Click,false)
/Cambiarlo por 
gwd.actions.events.addHandler("click-tag-1","action",gwd.auto_Click_tag_1Click,false)
gwd.actions.events.addHandler("click-tag-2","action",gwd.auto_Click_tag_2Click,false)
```


- Borrar las etiquetas html, head, body y metas, para que no interfiera con la página web.

El código final del archivo index.html sería:

```html

  <link href="http://concursos.crp.pe/CRP/demo/gwdpage_style.css" rel="stylesheet" data-version="12" data-exports-type="gwd-page">
  <link href="http://concursos.crp.pe/CRP/demo/gwdpagedeck_style.css" rel="stylesheet" data-version="11" data-exports-type="gwd-pagedeck">
  <link href="http://concursos.crp.pe/CRP/demo/gwddoubleclick_style.css" rel="stylesheet" data-version="18" data-exports-type="gwd-doubleclick">
  <link href="http://concursos.crp.pe/CRP/demo/gwdtaparea_style.css" rel="stylesheet" data-version="6" data-exports-type="gwd-taparea">
  <link href="http://concursos.crp.pe/CRP/demo/gwdimage_style.css" rel="stylesheet" data-version="12" data-exports-type="gwd-image">
  <style type="text/css" id="gwd-lightbox-style">.gwd-lightbox{overflow:hidden}</style>
  <style type="text/css" id="gwd-text-style">p{margin:0px}h1{margin:0px}h2{margin:0px}h3{margin:0px}</style>
  <link rel="stylesheet" href="http://concursos.crp.pe/CRP/demo/multimedia_expandible.css">
  <script data-source="http://concursos.crp.pe/CRP/demo/googbase_min.js" data-version="4" data-exports-type="googbase" src="http://concursos.crp.pe/CRP/demo/googbase_min.js"></script>
  <script data-source="http://concursos.crp.pe/CRP/demo/gwd_webcomponents_min.js" data-version="5" data-exports-type="gwd_webcomponents" src="http://concursos.crp.pe/CRP/demo/gwd_webcomponents_min.js"></script>
  <script data-source="http://concursos.crp.pe/CRP/demo/gwdpage_min.js" data-version="12" data-exports-type="gwd-page" src="http://concursos.crp.pe/CRP/demo/gwdpage_min.js"></script>
  <script data-source="http://concursos.crp.pe/CRP/demo/gwdpagedeck_min.js" data-version="11" data-exports-type="gwd-pagedeck" src="http://concursos.crp.pe/CRP/demo/gwdpagedeck_min.js"></script>
  <script data-source="https://s0.2mdn.net/ads/studio/Enabler.js" data-exports-type="gwd-doubleclick" src="https://s0.2mdn.net/ads/studio/Enabler.js"></script>
  <script data-source="http://concursos.crp.pe/CRP/demo/gwddoubleclick_min.js" data-version="18" data-exports-type="gwd-doubleclick" src="http://concursos.crp.pe/CRP/demo/gwddoubleclick_min.js"></script>
  <script data-source="http://concursos.crp.pe/CRP/demo/gwdtaparea_min.js" data-version="6" data-exports-type="gwd-taparea" src="http://concursos.crp.pe/CRP/demo/gwdtaparea_min.js"></script>
  <script data-source="http://concursos.crp.pe/CRP/demo/gwdimage_min.js" data-version="12" data-exports-type="gwd-image" src="http://concursos.crp.pe/CRP/demo/gwdimage_min.js"></script>
  <script type="text/javascript" gwd-events="support" src="http://concursos.crp.pe/CRP/demo/gwd-events-support.1.0.js"></script>

  <gwd-doubleclick id="gwd-ad" polite-load="">
    <gwd-metric-configuration>
      <gwd-metric-event source="expand-button" event="tapareaexit" metric="" exit="Exit"></gwd-metric-event>
      <gwd-metric-event source="close-button" event="tapareaexit" metric="" exit="Exit"></gwd-metric-event>
      <gwd-metric-event source="click-tag-1" event="tapareaexit" metric="" exit="Exit"></gwd-metric-event>
      <gwd-metric-event source="click-tag-2" event="tapareaexit" metric="" exit="Exit"></gwd-metric-event>
    </gwd-metric-configuration>
    <div is="gwd-pagedeck" class="gwd-page-container" id="pagedeck" default-page="banner-page" data-gwd-offset-top="0px" data-gwd-offset-left="0px">
      <div is="gwd-page" id="banner-page" data-gwd-name="Página de banner" class="gwd-page-wrapper banner gwd-lightbox" data-gwd-width="300px" data-gwd-height="250px" expanded="">
        <div class="gwd-page-content banner">
          <img is="gwd-image" source="http://concursos.crp.pe/CRP/demo/bg-replegado.jpg" id="gwd-image_2" class="gwd-img-x6ui">
          <img is="gwd-image" source="http://concursos.crp.pe/CRP/demo/btn-subscribe.png" id="gwd-image_5" class="gwd-img-s987">
          <gwd-taparea id="click-tag-1" class="gwd-taparea-179k"></gwd-taparea>
          <gwd-taparea id="expand-button" class="expand-button">
            <img is="gwd-image" source="http://concursos.crp.pe/CRP/demo/btn-expand.png" id="gwd-image_4" class="gwd-img-pyqh">
          </gwd-taparea>
        </div>
      </div>
      <div is="gwd-page" id="expanded-page" expanded="" data-gwd-name="Página expandida" class="gwd-page-wrapper expanded gwd-lightbox" data-gwd-width="600px" data-gwd-height="340px">
        <div class="gwd-page-content expanded">
          <img is="gwd-image" source="http://concursos.crp.pe/CRP/demo/bg-expandido.jpg" id="gwd-image_7" class="gwd-img-zegg">
          <gwd-taparea id="click-tag-2" class="gwd-taparea-339e"></gwd-taparea>
          <gwd-taparea id="close-button" class="close-button">
            <img is="gwd-image" source="http://concursos.crp.pe/CRP/demo/btn-close.png" id="gwd-image_6" class="gwd-img-qj9q">
          </gwd-taparea>
        </div>
      </div>
    </div>
    <gwd-exit metric="clicktag-1" url="%%CLICK_URL_UNESC%%%%DEST_URL%%"></gwd-exit>
    <gwd-exit metric="clicktag2" url="%%CLICK_URL_UNESC%%%%DEST_URL%%"></gwd-exit>
  </gwd-doubleclick>
  <script type="text/javascript" gwd-events="registration">gwd.actions.events.registerEventHandlers=function(event){gwd.actions.events.addHandler("expand-button","action",gwd.handleExpand_buttonAction,false);gwd.actions.events.addHandler("close-button","action",gwd.handleClose_buttonAction,false);gwd.actions.events.addHandler("close-button","action",gwd.handleClose_reportManualClose,false);gwd.actions.events.addHandler("document.body","adinitialized",gwd.auto_BodyAdinitialized,false);gwd.actions.events.addHandler("click-tag-1","action",gwd.auto_Click_tag_1Click,false);gwd.actions.events.addHandler("click-tag-2","action",gwd.auto_Click_tag_2Click,false)};gwd.actions.events.deregisterEventHandlers=function(event){gwd.actions.events.removeHandler("expand-button","action",gwd.handleExpand_buttonAction,false);gwd.actions.events.removeHandler("close-button","action",gwd.handleClose_buttonAction,false);gwd.actions.events.removeHandler("close-button","action",gwd.handleClose_reportManualClose,false);gwd.actions.events.removeHandler("document.body","adinitialized",gwd.auto_BodyAdinitialized,false);gwd.actions.events.removeHandler("click-tag-1","action",gwd.auto_Click_tag_1Click,false);gwd.actions.events.removeHandler("click-tag-2","action",gwd.auto_Click_tag_2Click,false)};document.addEventListener("DOMContentLoaded",gwd.actions.events.registerEventHandlers);document.addEventListener("unload",gwd.actions.events.deregisterEventHandlers)</script>
  <script type="text/javascript" gwd-events="handlers">gwd.handleExpand_buttonAction=function(){gwd.actions.gwdDoubleclick.goToPage("gwd-ad","expanded-page")};gwd.handleClose_buttonAction=function(){gwd.actions.gwdDoubleclick.goToPage("gwd-ad","banner-page")};gwd.handleClose_reportManualClose=function(){gwd.actions.gwdDoubleclick.reportManualClose("gwd-ad")};gwd.auto_BodyAdinitialized=function(event){gwd.actions.gwdPagedeck.goToPage("pagedeck","expanded-page","none",1e3,"linear","top")};gwd.auto_Click_tag_1Click=function(event){gwd.actions.gwdDoubleclick.exit("gwd-ad","clicktag-1","%%CLICK_URL_UNESC%%%%DEST_URL%%",true,true,"banner-page")};gwd.auto_Click_tag_2Click=function(event){gwd.actions.gwdDoubleclick.exit("gwd-ad","clicktag2","%%CLICK_URL_UNESC%%%%DEST_URL%%",true,true,"banner-page")}</script>
  <script type="text/javascript" id="gwd-init-code">
    (function() {
      var gwdAd = document.getElementById('gwd-ad');
      function handleDomContentLoaded(event) {
      }
      function handleWebComponentsReady(event) {
        setTimeout(function() {
          gwdAd.initAd();
        }, 0);
      }
      function handleAdInitialized(event) {}
      window.addEventListener('DOMContentLoaded',
        handleDomContentLoaded, false);
      window.addEventListener('WebComponentsReady',
        handleWebComponentsReady, false);
      window.addEventListener('adinitialized',
        handleAdInitialized, false);
    })();
  </script>
    <script data-exports-type="gwd-studio-registration">function StudioExports() {
    Enabler.setExpandingPixelOffsets(0,0,600,340,false,false);
    Enabler.exit("clicktag-1", "%%CLICK_URL_UNESC%%%%DEST_URL%%");
    Enabler.exit("clicktag2", "%%CLICK_URL_UNESC%%%%DEST_URL%%");
    }</script><script type="text/gwd-admetadata">{"version":1,"type":"DoubleClick","format":"","template":"Expandable 3.0.0","politeload":true,"fullscreen":false,"counters":[],"timers":[],"exits":[{"exitId":"clicktag-1","url":"%%CLICK_URL_UNESC%%%%DEST_URL%%"},{"exitId":"clicktag2","url":"%%CLICK_URL_UNESC%%%%DEST_URL%%"}],"creativeProperties":{"minWidth":300,"minHeight":250,"maxWidth":600,"maxHeight":340},"components":["gwd-doubleclick","gwd-image","gwd-page","gwd-pagedeck","gwd-taparea"],"responsive":false}</script>
```

